@extends('layouts.app')

@section('content')
<div class="row container my-3">
	<div class="col-2"></div>
	<div class="col-8 text-center">
		<h2 class="text-center">this is the search result</h2>
      <table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
     
    </tr>
  </thead>
  <tbody>
@foreach($result as $value)
    <tr>
      <th scope="row">{{$value->id}}</th>
      <td>{{$value->name}}</td>
      <td>{{$value->email}}</td>
    </tr>
@endforeach
  </tbody>
</table>
</div>
<div class="col-2"></div>	
</div>
@endsection