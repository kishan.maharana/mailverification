@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-12 text-center">

	<form class="form-control" action="{{url('/image')}}" method="post" enctype="multipart/form-data">
		{{csrf_field()}}
		<input type="file" name="image" class="form-control">
		<button type="submit" class="btn btn-success">submit</button>
	</form>
	</div>
</div>

@endsection