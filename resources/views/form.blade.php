@extends('layouts.app')

@section('content')

<form class="container" action="{{url('/store')}}" method="post">

{{csrf_field()}}
	


@if(count($errors))
      @foreach($errors->all() as $error)
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
  			{{$error}}
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    		<span aria-hidden="true">&times;</span>
  			</button>
		  </div>
      @endforeach
@endif


	@if(session('successmessage'))
		<div class="alert alert-success alert-dismissible fade show" role="alert">
  			{{session('successmessage')}}
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    		<span aria-hidden="true">&times;</span>
 			</button>
		</div>
	@endif


	<label>Name</label>
	<input type="text" name="name" class="form-control">
	<label>E-Mail</label>
	<input type="email" name="email" class="form-control">
	<label>Password</label>
	<input type="password" name="password" class="form-control"> 
	<label>Address</label>
	<input type="address" name="address" class="form-control">
	<label>Phone No.</label>
	<input type="number" name="phone" class="form-control">
	<label>About</label>
	<textarea class="form-control" rows="3" name="detail"></textarea>
	<button type="submit" class="btn btn-primary">submit</button>

</form>

@endsection