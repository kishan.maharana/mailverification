<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */ 
    public function index()
    {
        $data=User::all();
        return view('home',compact('data'));
    }
    public function search(Request $req)
    {
        $search=$req->get('search');
        $result=DB::table('users')->where('name','like','%'.$search.'%')->get();
        return view('searchdata',compact('result'));
    }
}
