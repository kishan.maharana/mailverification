<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\Detail;
use Hash;
use DB;
class checkController extends Controller
{
    public function index()
    {
    	$data=DB::table('products')
    		  ->whereTime('created_at','13:35:19')
    		  ->get();

    	dd($data);
    }
    public function demo()
    {
    	$str="this is an array";
		
		$result=explode(" ",$str);

    	return view('demo',compact('result'));
    }
    public function store(Request $req)
    {
        $this->validate($req,[
                'name' => 'required|min:5|max:35',
                'email' => 'required|email|unique:users',
                'phone' => 'required|numeric',
                'password' => 'required|min:2|max:20',
                'address' => 'required|min:5|max:500',
                'detail' => 'required|min:5|max:1000',
            ],[
                'name.required' => ' The Name field is required.',
                'email.min' => ' Enter a proper email address',
                'phone.min' => ' Phone should be atleast 10 digits',
                'phone.max' => ' Phone should maximum 10 digits',
                'password.required' => 'password field is required',
                'address.required' => ' Address field is required',
                'detail.required' => ' Detail field is required',
                'detail.min' => ' Enter atleast 5 characters',
                
            ]);
        $data=new Detail;
        $data->name=$req->name;
        $data->email=$req->email;
        $data->phone=$req->phone;
        $data->password=Hash::make($req->address);
        $data->address=$req->address;
        $data->detail=$req->detail;
       
        $data->save();
        
        if($data->save())
        {
            return redirect(url('/form'))->with('successmessage','data saved successfully');
        }
}
}
