<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class imageController extends Controller
{
    public function index()
    {
    	return view('image');
    }
    public function blur(Request $request)
    {
    	 if($request->hasfile('image'))
        {
            $file=$request->file('image');
            $extension=$file->getClientOriginalExtension();
            $filename=time().'.'.$extension;
            $image = Image::make($file->getRealPath());              
            $image->greyscale();
            $image->crop(1200, 500,400,50);
    		$image->blur(15);
            $image->save('blured.png',5);

            // return $image->response('png');
            // $file->move('public/upload/resized/',$filename);
            dd('Image Manipulated');
        }
        else
        {

            dd('Image not Manipulated');
        }
    }
}
