<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class ApiController extends Controller
{
    public function allPostWithUser($pid)
    {
    	   $data = DB::table('users')
            ->join('products', 'users.id', '=', 'products.userid')
            ->select('users.*', 'products.price', 'products.category')
            ->where('products.id','=',$pid)
            ->get();

		return $data;
    }
}
